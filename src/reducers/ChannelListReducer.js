import { MY_CHANNELS_LIST_RECEIVED, PUBLIC_CHANNELS_LIST_RECEIVED } from '../actions/types';

const INITIAL_STATE = {
  myChannels: [],
  publicChannels: []
};

export default (state = INITIAL_STATE, action) => {
  console.log(`value of action type is${action.type}`);

  switch (action.type) {
    case MY_CHANNELS_LIST_RECEIVED:
    case PUBLIC_CHANNELS_LIST_RECEIVED:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};
