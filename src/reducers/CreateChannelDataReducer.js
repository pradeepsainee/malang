import {
  FRIEND_INPUT_CHANGED,
  SELECT_FRIEND_ID,
  FB_FRIENDS_USING_APP_RECEIVED
} from '../actions/types';

const INITIAL_STATE = {
  friend: '',
  data: '',
  selectedFriendId: '',
  fbFriends: []
};

export default (state = INITIAL_STATE, action) => {
  console.log(`value of action type is${action.type}`);

  switch (action.type) {
    case FRIEND_INPUT_CHANGED:
    case SELECT_FRIEND_ID:
    case FB_FRIENDS_USING_APP_RECEIVED:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};
