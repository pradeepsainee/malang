import { Router } from '../components/ReduxNavigation';

const initialState = Router.router.getStateForAction(
  Router.router.getActionForPathAndParams('LoginScreen')
);

export const navReducer = (state = initialState, action) => {
  console.log(`value of action type is${action.type}`);

  const nextState = Router.router.getStateForAction(action, state);

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
};
