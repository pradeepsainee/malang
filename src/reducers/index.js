import { combineReducers } from 'redux';
import CreateChannelDataReducer from './CreateChannelDataReducer';
import AuthReducer from './AuthReducer';
import { navReducer } from './NavReducer';
import { CurrentChannelReducer } from './CurrentChannelReducer';
import ChannelListReducer from './ChannelListReducer';
import SpinnerReducer from './SpinnerReducer';
import TabReducer from './TabReducer';

const masterReducer = combineReducers({
  createChannelData: CreateChannelDataReducer,
  authData: AuthReducer,
  navData: navReducer,
  currentChannelData: CurrentChannelReducer,
  channelListData: ChannelListReducer,
  spinner: SpinnerReducer,
  tabView: TabReducer
});

export default masterReducer;
