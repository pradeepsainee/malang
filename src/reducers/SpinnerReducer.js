import { SHOW_SPINNER } from '../actions/types';

const INITIAL_STATE = {
  showSpinner: false
};

export default (state = INITIAL_STATE, action) => {
  console.log(`value of action type is${action.type}`);

  switch (action.type) {
    case SHOW_SPINNER:
      return {
        ...state,
        showSpinner: action.payload
      };

    default:
      return state;
  }
};
