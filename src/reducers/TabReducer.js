import { CHANGE_INDEX } from '../actions/types';

const INITIAL_STATE = {
  index: 0,
  routes: [
    { key: 'first', title: 'Live' },
    { key: 'second', title: 'SHARE/DEDICATE' },
    // { key: 'three', title: 'Dedicate' }
  ]
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CHANGE_INDEX:
      return { ...state, index: action.payload };
    default:
      return state;
  }
};
