import {
  SELECT_CURRENT_CHANNEL,
  ON_SONG_SEARCH_INPUT_CHANGED,
  ON_SONGSUGGESTION_SELECTED,
  ON_SONG_SELECTED,
  CHANNEL_MEMBERS,
  CHANNEL_LISTENERS,
  ON_SHARED_OR_DED_SONGS_RECEIVED,
  ON_CHAT_RECEIVED,
  CHANGE_INDEX,
  SET_MESSAGE,
  CLEAR_PREV_CHANNEL_DATA
} from '../actions/types';

const INITIAL_STATE = {
  currentSong: { currentSongPicUrl: null },
  channelId: '',
  private: null,
  members: {},
  suggestions: [],
  searchResults: [],
  text: '',
  currentSongPicUrl: '',
  hideResults: false,
  listeners: {},
  sharedAndDedSongs: [],

  chatMessages: [],

  index: 0,
  chat: [],
  message: ''
};

export const CurrentChannelReducer = (state = INITIAL_STATE, action) => {
  console.log(`current channel state${JSON.stringify(state)}`);
  switch (action.type) {
    case SELECT_CURRENT_CHANNEL:
      return { ...state, channelId: action.channelId };
    case ON_SONG_SEARCH_INPUT_CHANGED:
    case ON_SONGSUGGESTION_SELECTED:
      return { ...state, ...action.payload };
    case ON_SONG_SELECTED:
      return { ...state, currentSong: action.payload, searchResults: [] };
    case CHANNEL_MEMBERS:
      return { ...state, members: action.payload };
    case CHANGE_INDEX:
      return { ...state, index: action.payload };
    case CHANNEL_LISTENERS:
      return { ...state, listeners: action.listeners };
    case ON_SHARED_OR_DED_SONGS_RECEIVED:
      return { ...state, sharedAndDedSongs: action.payload };
    case ON_CHAT_RECEIVED:
      return {
        ...state,
        chat: state.chat.length === 0 ? [action.payload] : state.chat.concat(action.payload)
      };
    case SET_MESSAGE:
      return { ...state, message: action.payload };
    case CLEAR_PREV_CHANNEL_DATA:
      return { ...INITIAL_STATE };
    default:
      return state;
  }
};
