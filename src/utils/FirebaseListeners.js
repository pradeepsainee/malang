import firebase from 'firebase';
import RNAudioStreamer from 'react-native-audio-streamer';
import {
  // SELECT_CURRENT_CHANNEL,
  // ON_SONG_SEARCH_INPUT_CHANGED,
  // ON_SONGSUGGESTION_SELECTED,
  ON_SONG_SELECTED
  // CHANNEL_MEMBERS,
  // CHANNEL_LISTENERS,
  // ON_SHARED_OR_DED_SONGS_RECEIVED,
  // SHOW_SPINNER
} from '../actions/types';

//listen for change in currentSong streamUrl value
export const newSongListener = (channelId, dispatch) => {
  //this FirebaseListener is only attached to syncedListener
  //on value change..fire a callback to read currentSong node and then
  //use that data to play music on device
  console.log('song listener attached!');
  console.log(`channelId is${channelId}`);
  firebase
    .database()
    .ref(`channels/${channelId}/currentSong`)
    .on('value', snapshot => {
      console.log(snapshot.val());
      if (snapshot.val() == null) {
        console.log('no currentSong exists!');
      }
      if (snapshot.val()) {
        dispatch({ type: ON_SONG_SELECTED, payload: snapshot.val() });

        const streamUrl = `${snapshot.val().streamUrl}?client_id=P6Zp6A76je8pgNLTe6a7I8rh3EKQRXnr`;
        const time = snapshot.val().lastUpdatedT;
        console.log(`playing song...${streamUrl}`);
        RNAudioStreamer.setUrl(streamUrl);

        RNAudioStreamer.play();
        console.log(`time is ${time}`);
        if (time) RNAudioStreamer.seekToTime(time);
      }
    });
};

//listens for change in lastUpdatedT of currentSong of channel
export const songTimeListener = (channelId, timeListenerAdded) => {
  console.log('songtimelistener attached!');

  firebase
    .database()
    .ref(`channels/${channelId}/currentSong/lastUpdatedT`)
    .on('value', snapshot => {
      let timeAdded = null;

      firebase
        .database()
        .ref(`channels/${channelId}/currentSong/time_added`)
        .once('value', timeAddedSnapshot => {
          timeAdded = timeAddedSnapshot.val();
          console.log(
            `time song updated was ${timeAdded}and time you joined was ${timeListenerAdded}`
          );
          if (timeAdded !== null) {
            if (timeAdded > timeListenerAdded) {
              attachRelevantListeners(true, channelId, timeListenerAdded);
            }
            // if (timeAdded > timeListenerAdded && snapshot.val() === '987654321') {
            //   attachRelevantListeners(true, channelId, timeListenerAdded);
            // }
          }
        });
    });
};

//listens for new Asynced member
//attached only to syncedListener
export const newMusicListenerAddedListener = channelId => {
  //switch off other listeners

  console.log('songtimelistener turned off!');

  firebase
    .database()
    .ref(`channels/${channelId}/listeners`)
    .on('value', snapshot => {
      console.log('firing newMusicListenerAdded listener');

      RNAudioStreamer.status((err, status) => {
        if (!err) {
          console.log(status);
          if (status === 'PLAYING') {
            RNAudioStreamer.currentTime((error, currentTimeOfSong) => {
              if (!error) {
                console.log(`the currentTime is${currentTimeOfSong}`); //seconds
                firebase
                  .database()
                  .ref(`channels/${channelId}/currentSong`)
                  .update({
                    lastUpdatedT: currentTimeOfSong,
                    time_added: firebase.database.ServerValue.TIMESTAMP
                  });
              }
            });
          }
          if (status === 'FINISHED') {
            RNAudioStreamer.currentTime((error, currentTimeOfSong) => {
              if (!error) {
                console.log(`the currentTime is${currentTimeOfSong}`); //seconds
                firebase
                  .database()
                  .ref(`channels/${channelId}/currentSong`)
                  .update({
                    lastUpdatedT: 987654321,
                    time_added: firebase.database.ServerValue.TIMESTAMP
                  });
              }
            });
          }
        }
      });

      //get playtime from musicplayer and update
      //lastUpdatedT for the currentSong
    });
};

export const attachRelevantListeners = (syncState, channelId, timeListenerAdded, dispatch) => {
  if (syncState === false) {
    songTimeListener(channelId, timeListenerAdded);
  }
  if (syncState === true) {
    firebase
      .database()
      .ref(`channels/${channelId}/currentSong/lastUpdatedT`)
      .off();

    newSongListener(channelId, dispatch);
    newMusicListenerAddedListener(channelId);
  }
};
