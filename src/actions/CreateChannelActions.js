import {
  FRIEND_INPUT_CHANGED,
  SELECT_FRIEND_ID,
  FB_FRIENDS_USING_APP_RECEIVED,
  SHOW_SPINNER
} from './types';

import firebase from 'firebase';
import { Router } from '../components/ReduxNavigation';
import { SelectCurrentChannelAction } from './CurrentChannelActions';

const FBSDK = require('react-native-fbsdk');

const { GraphRequest, GraphRequestManager } = FBSDK;

export const SuggestFriendAction = text => dispatch => {
  firebase
    .database()
    .ref('users')
    .once('value')
    .then(snapshot => {
      dispatch({
        type: FRIEND_INPUT_CHANGED,
        payload: { friend: text, data: snapshot.val() }
      });
    });
};

export const SelectFriendIdAction = (text, selectedFriendId) => ({
  type: SELECT_FRIEND_ID,
  payload: { friend: text, selectedFriendId, data: '' }
});

export const CreateChannelFbAction = (
  selectedFriendId,
  friendName,
  friendPhoto,
  friendFbId
) => dispatch => {
  const userId = firebase.auth().currentUser.uid;

  console.log(`friend id is${selectedFriendId}`);

  const ref = firebase.database().ref();
  const username = firebase.auth().currentUser.email;
  const userFbId = firebase.auth().currentUser.providerData[0].uid;
  const channelRef = ref.child('channels');

  firebase
    .database()
    .ref(`createdChannelsFb/${userId}/${friendFbId}`)
    .once('value', snapshot => {
      if (snapshot.val()) {
        console.log(`already exists${JSON.stringify(snapshot.val())}`);
        dispatch(SelectCurrentChannelAction(snapshot.val()));
      } else {
        const channelKey = channelRef.push().key;
        const updatedUserData = {};
        const globalChannelName = `${username} ${friendName}`;
        updatedUserData[`channels/${channelKey}`] = {
          globalChannelName,
          members: {
            [userId]: { pic: firebase.auth().currentUser.photoURL, name: username },
            [selectedFriendId]: { pic: friendPhoto, name: friendName }
          },
          private: true,
          currentSong: { streamUrl: null, lastUpdatedT: null, currentSongPicUrl: null },
          playHistory: null,
          listeners: null, //[userId]:{syncState:boolean}
          globalSyncState: null,
          id_name: `${selectedFriendId}_${userId}`
        };

        updatedUserData[`userChannels/${userId}/${channelKey}`] = {
          name: friendName,
          private: true,
          active: true,
          members: { [userId]: true, [selectedFriendId]: true },
          photo: friendPhoto
        };

        updatedUserData[`userChannels/${selectedFriendId}/${channelKey}`] = {
          name: username,
          private: true,
          active: false,
          members: { [userId]: true, [selectedFriendId]: true },
          photo: firebase.auth().currentUser.photoURL
        };

        updatedUserData[`createdChannelsFb/${userId}/${friendFbId}`] = channelKey;
        updatedUserData[`createdChannelsFb/${selectedFriendId}/${userFbId}`] = channelKey;

        ref.update(updatedUserData, () => {
          dispatch(SelectCurrentChannelAction(channelKey));
        });
      }
    });

  // firebase
  //   .database()
  //   .ref('channels/')
  //   .orderByChild('id_name')
  //   .equalTo(`${userId}_${selectedFriendId}`)
  //   .once('value', snapshot => {
  //     if (snapshot.val()) {
  //       console.log(`already exists${JSON.stringify(snapshot.val())}`);
  //       dispatch(SelectCurrentChannelAction(Object.keys(snapshot.val())[0]));
  //     } else {
  //       firebase
  //         .database()
  //         .ref('channels/')
  //         .orderByChild('id_name')
  //         .equalTo(`${selectedFriendId}_${userId}`)
  //         .once('value', snapshot2 => {
  //           if (snapshot2.val()) {
  //             console.log(`already exists${JSON.stringify(snapshot2.val())}`);
  //             dispatch(SelectCurrentChannelAction(Object.keys(snapshot2.val())[0]));
  //           } else {
  //             const channelKey = channelRef.push().key;

  //             const updatedUserData = {};
  //             const globalChannelName = `${username} ${friendName}`;
  //             updatedUserData[`channels/${channelKey}`] = {
  //               globalChannelName,
  //               members: {
  //                 [userId]: { pic: firebase.auth().currentUser.photoURL, name: username },
  //                 [selectedFriendId]: { pic: friendPhoto, name: friendName }
  //               },
  //               private: true,
  //               currentSong: { streamUrl: null, lastUpdatedT: null, currentSongPicUrl: null },
  //               playHistory: null,
  //               listeners: null, //[userId]:{syncState:boolean}
  //               globalSyncState: null,
  //               id_name: `${selectedFriendId}_${userId}`
  //             };

  //             updatedUserData[`userChannels/${userId}/${channelKey}`] = {
  //               name: friendName,
  //               private: true,
  //               active: true,
  //               members: { [userId]: true, [selectedFriendId]: true },
  //               photo: friendPhoto
  //             };

  //             updatedUserData[`userChannels/${selectedFriendId}/${channelKey}`] = {
  //               name: username,
  //               private: true,
  //               active: false,
  //               members: { [userId]: true, [selectedFriendId]: true },
  //               photo: firebase.auth().currentUser.photoURL
  //             };

  //             updatedUserData[`createdChannelsFb/${userId}/${friendFbId}`] = channelKey;
  //             updatedUserData[`createdChannelsFb/${selectedFriendId}/${userFbId}`] = channelKey;

  //             ref.update(updatedUserData, () => {
  //               dispatch(SelectCurrentChannelAction(channelKey));
  //             });
  //           }
  //         });
  //     }
  //   });
};

export const getFbFriendsUsingAppAction = () => dispatch => {
  // Create a graph request asking for user information with a callback to handle the response.
  dispatch({ type: SHOW_SPINNER, payload: true });
  const infoRequest = new GraphRequest('/me/friends', null, (error, result) => {
    const arr = [];
    if (error) {
      console.log(`Error fetching data: ${error.toString()}`);
    } else {
      console.log(`Success fetching data: ${JSON.stringify(result.data)}`);

      result.data.forEach(s => {
        console.log(`fb fiends${JSON.stringify(s)}`);

        firebase
          .database()
          .ref('users/')
          .orderByChild('fb_id')
          .equalTo(s.id)
          .once('value', snapshot => {
            arr.push(snapshot.val());
            console.log(arr);

            //this is a temporary fix. got to write better algo
            if (arr.length === result.data.length) {
              dispatch({ type: SHOW_SPINNER, payload: false });
              dispatch({ type: FB_FRIENDS_USING_APP_RECEIVED, payload: { fbFriends: arr } });
            }
          });
      });
    }
  });
  // Start the graph request.
  new GraphRequestManager().addRequest(infoRequest).start();
};

// export const CreateChannelAction = (selectedFriendId, friend) => dispatch => {
//   const userId = firebase.auth().currentUser.uid;

//   console.log(`friend id is${selectedFriendId}`);

//   const ref = firebase.database().ref();
//   const username = firebase.auth().currentUser.email;
//   const channelRef = ref.child('channels');
//   const channelKey = channelRef.push().key;

//   const updatedUserData = {};
//   const globalChannelName = `${username} ${friend}`;
//   updatedUserData[`channels/${channelKey}`] = {
//     globalChannelName,
//     members: { [userId]: true, [selectedFriendId]: true },
//     private: true,
//     currentSong: { streamUrl: null, lastUpdatedT: null, currentSongPicUrl: null },
//     playHistory: null,
//     listeners: null, //[userId]:{syncState:boolean}
//     globalSyncState: null
//   };

//   updatedUserData[`userChannels/${userId}/${channelKey}`] = {
//     name: friend,
//     private: true,
//     active: true,
//     members: { [userId]: true, [selectedFriendId]: true }
//   };

//   updatedUserData[`userChannels/${selectedFriendId}/${channelKey}`] = {
//     name: username,
//     private: true,
//     active: false,
//     members: { [userId]: true, [selectedFriendId]: true }
//   };

//   ref.update(updatedUserData, () => {
//     dispatch(SelectCurrentChannelAction(channelKey));
//   });
// };
