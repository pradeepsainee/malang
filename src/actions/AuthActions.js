import { EMAIL_CHANGED, PASSWORD_CHANGED, LOGIN_SUCCESS, LOGIN_FAILURE, LOGIN_USER } from './types';

import { Router } from '../components/ReduxNavigation';

import firebase from 'firebase';

const FBSDK = require('react-native-fbsdk');

export const passwordChanged = password => ({ type: PASSWORD_CHANGED, payload: password });

export const emailChanged = email => ({ type: EMAIL_CHANGED, payload: email });

export const loginUser = (email, password) => dispatch => {
  dispatch({ type: LOGIN_USER });
  console.log(email);
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(user => loginUserSuccess(dispatch, user))
    .catch(error => {
      console.log(error);

      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(user => createUserNode(dispatch, user))
        .catch(myerror => loginUserFail(dispatch, myerror));
    });
};

const loginUserFail = (dispatch, error) => {
  console.log(error);
  dispatch({ type: LOGIN_FAILURE });
};

const createUserNode = (dispatch, user) => {
  const ref = firebase.database().ref(`users/${user.uid}/`);
  ref.set({
    name: user.displayName,
    channels: null,
    photo: user.photoUrl
  });
  loginUserSuccess(dispatch, user);
};

export const createUserNodeFacebook = user => dispatch => {
  const ref = firebase.database().ref(`users/${user.uid}/`);
  ref.set({
    name: user.displayName,
    channels: null,
    photo: user.photoURL,
    fb_id: user.providerData[0].uid
  });
  // loginUserSuccess(dispatch, user);
};

export const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_SUCCESS,
    payload: user
  });
  console.log(Router.router.getActionForPathAndParams('ChannelListScreen'));
  dispatch(Router.router.getActionForPathAndParams('ChannelListScreen'));
};

export const loginUserSuccessFB = user => dispatch => {
  console.log('fb login!!');
  dispatch({
    type: LOGIN_SUCCESS,
    payload: user
  });
  console.log(Router.router.getActionForPathAndParams('ChannelListScreen'));
  dispatch(Router.router.getActionForPathAndParams('ChannelListScreen'));
};

export const loginScreenNavAction = () => dispatch => {
  dispatch(Router.router.getActionForPathAndParams('LoginScreen'));
};
