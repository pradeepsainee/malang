import { MY_CHANNELS_LIST_RECEIVED, PUBLIC_CHANNELS_LIST_RECEIVED, SHOW_SPINNER } from './types';
import firebase from 'firebase';
import { Router } from '../components/ReduxNavigation';

export const getMyChannelsAction = () => dispatch => {
  const userId = firebase.auth().currentUser.uid;
  dispatch({ type: SHOW_SPINNER, payload: true });
  firebase
    .database()
    .ref(`userChannels/${userId}`)
    .once('value', snapshot => {
      const value = snapshot.val() || {};
      const myChannelsArray = Object.keys(value).map(key => ({ channelId: key, ...value[key] }));
      console.log('successfully received channel lists from firebase!');
      dispatch({ type: SHOW_SPINNER, payload: false });
      dispatch({
        type: MY_CHANNELS_LIST_RECEIVED,
        payload: { myChannels: myChannelsArray }
      });
    });
};

export const navigateToCreateChannelScreen = () => dispatch => {
  console.log('hi!');
  dispatch(Router.router.getActionForPathAndParams('CreateChannelScreen'));
};

export const getPublicChannelsAction = userId => dispatch => {};
