import {
  SELECT_CURRENT_CHANNEL,
  ON_SONG_SEARCH_INPUT_CHANGED,
  ON_SONGSUGGESTION_SELECTED,
  ON_SONG_SELECTED,
  CHANNEL_MEMBERS,
  CHANNEL_LISTENERS,
  ON_SHARED_OR_DED_SONGS_RECEIVED,
  SHOW_SPINNER,
  CHANGE_INDEX,
  ON_CHAT_RECEIVED,
  SET_MESSAGE,
  CLEAR_PREV_CHANNEL_DATA
} from './types';

import { Keyboard } from 'react-native';

import firebase from 'firebase';
import RNAudioStreamer from 'react-native-audio-streamer';
import { Router } from '../components/ReduxNavigation';

import {
  // newSongListener,
  // songTimeListener,
  // globalSyncStateListener,
  attachRelevantListeners
} from '../utils/FirebaseListeners';

// import {}

export const SelectCurrentChannelAction = channelId => dispatch => {
  dispatch({ type: SHOW_SPINNER, showSpinner: true });
  dispatch({ type: CLEAR_PREV_CHANNEL_DATA });
  dispatch(Router.router.getActionForPathAndParams('ChannelScreen'));
  const userId = firebase.auth().currentUser.uid;

  //determine syncState (on basis of whether currentSong is null or not)

  dispatch({ type: SELECT_CURRENT_CHANNEL, channelId });

  getSongsSharedOrDedicatedAction(channelId, dispatch);

  const ref = firebase.database().ref();

  ref
    .child(`channels/${channelId}/listeners/${userId}`)
    .onDisconnect()
    .remove();

  ref.child(`channels/${channelId}/listeners`).on('value', snapshot => {
    // console.log(`listeners are${JSON.stringify(snapshot.val())}`);

    let listeners = null;

    if (snapshot.val()) {
      listeners = snapshot.val();
    } else if (snapshot.val() == null) {
      listeners = {};
    }
    dispatch({ type: CHANNEL_LISTENERS, listeners });
  });

  ref
    .child(`channels/${channelId}/listeners/${userId}`)
    .update({
      name: firebase.auth().currentUser.email,
      time_added: firebase.database.ServerValue.TIMESTAMP
    })
    .then(data => {
      // console.log(`listeners are ${JSON.stringify(data)}`);

      ref.child(`channels/${channelId}`).once('value', snapshot => {
        let syncState = null;
        const timeAdded = snapshot.val().listeners[userId].time_added;

        const listeners = snapshot.val().listeners || {};
        // console.log(`listeners length are ${JSON.stringify(Object.keys(listeners).length)}`);
        // console.log(snapshot.val().currentSong);
        const members = snapshot.val().members;
        dispatch({ type: CHANNEL_MEMBERS, payload: members });

        // console.log(`time of addition on server${timeAdded}`);
        if (snapshot.val().currentSong && Object.keys(listeners).length > 1) {
          // console.log('currentSong exists or so syncState is false');
          syncState = false;
        }
        if (snapshot.val().currentSong && Object.keys(listeners).length === 1) {
          // console.log('currentSong exist but no previous listener syncState is true');
          syncState = true;
        }
        if (snapshot.val().currentSong == null) {
          // console.log("currentSong doesn't exist so syncState is true");
          syncState = true;
        }
        // console.log(`syncstate is ${syncState}`);
        attachRelevantListeners(syncState, channelId, timeAdded, dispatch);
        // });
      });
    })
    .catch(error => {
      // console.log(`error updating listeners syncState${error}`);
    });
};

export const onSongSearchInputTextChangedAction = text => dispatch => {
  if (text.length % 3 === 0) {
    const fetchEndpoint = `https://api-v2.soundcloud.com/search/queries?client_id=P6Zp6A76je8pgNLTe6a7I8rh3EKQRXnr&limit=10&q=${text}`;
    fetch(fetchEndpoint)
      .then(response => response.json())
      .then(response => {
        dispatch({
          type: ON_SONG_SEARCH_INPUT_CHANGED,
          payload: { text, suggestions: response.collection, hideResults: false }
        });
      })
      .catch(error => {
        console.log(error);
      });
  }
};

export const onSongSuggestionSelectedAction = text => dispatch => {
  //close keyboard
  Keyboard.dismiss();

  const fetchEndpoint = `https://api.soundcloud.com/tracks?client_id=P6Zp6A76je8pgNLTe6a7I8rh3EKQRXnr&limit=10&q=${text}`;

  fetch(fetchEndpoint)
    .then(response => response.json())
    .then(response => {
      dispatch({
        type: ON_SONGSUGGESTION_SELECTED,
        payload: { text: '', searchResults: response, suggestions: [], hideResults: true }
      });
    })
    .catch(error => {
      // console.log(error);
    });
};

//update currentSong for the channel in database
export const onSongSelectedAction = (channelId, selectedSong) => dispatch => {
  // console.log(`selectedSong on onsongselectedaction is ${selectedSong}`);

  firebase
    .database()
    .ref(`channels/${channelId}`)
    .update({
      currentSong: {
        trackName: selectedSong.title,
        uploaderName: selectedSong.user.username,
        streamUrl: selectedSong.stream_url,
        currentSongPicUrl: selectedSong.artwork_url,
        playedBy: firebase.auth().currentUser.displayName
      }
    })
    .then(() => {
      firebase
        .database()
        .ref(`channels/${channelId}/currentSong`)
        .once('value', snapshot => {
          dispatch({ type: ON_SONG_SELECTED, payload: snapshot.val() });
        });
    });
};

export const onSongSharedAction = (channelId, selectedSong) => dispatch => {
  // console.log(`selectedSong on onSongsharedAction is ${selectedSong}`);

  firebase
    .database()
    .ref(`channels/${channelId}/shared`)
    .push({
      shared: true,
      trackName: selectedSong.title,
      streamUrl: selectedSong.stream_url,
      currentSongPicUrl: selectedSong.artwork_url,
      sharee: firebase.auth().currentUser.displayName
    })
    .then(() => {
      // firebase
      //   .database()
      //   .ref(`channels/${channelId}/currentSong`)
      //   .once('value', snapshot => {
      //     dispatch({ type: ON_SONG_SELECTED, payload: snapshot.val() });
      //   });
    });
};

export const onSongDedicatedAction = (channelId, selectedSong) => dispatch => {
  // console.log(`selectedSong on onSongDedicatedAction is ${selectedSong}`);

  firebase
    .database()
    .ref(`channels/${channelId}/shared`)
    .push({
      dedicated: true,
      trackName: selectedSong.title,
      streamUrl: selectedSong.stream_url,
      currentSongPicUrl: selectedSong.artwork_url,
      dedicator: firebase.auth().currentUser.displayName,
      dedicated_to: 'pradeep saini',
      caption: 'random dedicatory message'
    })
    .then(() => {
      // firebase
      //   .database()
      //   .ref(`channels/${channelId}/currentSong`)
      //   .once('value', snapshot => {
      //     dispatch({ type: ON_SONG_SELECTED, payload: snapshot.val() });
      //   });
    });
};

export const getSongsSharedOrDedicatedAction = (channelId, dispatch) => {
  firebase
    .database()
    .ref(`channels/${channelId}/shared`)
    .limitToLast(10)
    .once('value', snapshot => {
      // console.log(`shaed and ded songs${snapshot.val()}`);
      const obj = snapshot.val() ? snapshot.val() : {};
      const arr = Object.keys(obj).map(key => obj[key]);

      dispatch({ type: ON_SHARED_OR_DED_SONGS_RECEIVED, payload: arr });
    });
};

export const sendMessageAction = (channelId, message) => dispatch => {
  const user = firebase.auth().currentUser;
  firebase
    .database()
    .ref(`channels/${channelId}/chat`)
    .push({
      message,
      senderId: user.uid,
      senderName: user.displayName
    })
    .then(dispatch(onChangeChatInputTextAction('')));
};

export const onChangeChatInputTextAction = message => ({
  type: 'SET_MESSAGE',
  payload: message
});

export const getChatMessagesAction = (channelId, chat) => dispatch => {
  firebase
    .database()
    .ref(`channels/${channelId}/chat`)

    .on('child_added', snapshot => {
      // console.log(`chat messages ${JSON.stringify(snapshot.val())}`);
      const obj = snapshot.val() ? snapshot.val() : {};
      // console.log(`snapshot from chat${JSON.stringify(snapshot)}`);

      // console.log(arr);
      dispatch({ type: ON_CHAT_RECEIVED, payload: obj });
    });
};

export const changeIndexAction = index => ({ type: CHANGE_INDEX, payload: index });
