import { CHANGE_INDEX } from './types';

export const changeIndexAction = index => ({ type: CHANGE_INDEX, payload: index });
