import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import SplashScreen from 'react-native-smart-splash-screen';
import firebase from 'firebase';
import reducers from './reducers';
import ReduxThunk from 'redux-thunk';
import ReduxNavigation from './components/ReduxNavigation';
import CurrentChannelListener from './components/CurrentChannelListener';

import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware
} from 'react-navigation-redux-helpers';

// const {
//   LoginButton,

//   AccessToken
// } = FBSDK;

const NavMiddleware = createReactNavigationReduxMiddleware('root', state => state.nav);
export const addListener = createReduxBoundAddListener('root');
const middleware = [ReduxThunk, NavMiddleware];

class App extends Component {
  componentWillMount() {
  
    const config = {
      apiKey: 'AIzaSyAVOC0_8eqdNxAgvVfMwBQGhT97ZiodQIw',
      authDomain: 'malang-4a5ca.firebaseapp.com',
      databaseURL: 'https://malang-4a5ca.firebaseio.com',
      projectId: 'malang-4a5ca',
      storageBucket: '',
      messagingSenderId: '706127195219'
    };

    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }

    console.ignoredYellowBox = [
      'Setting a timer for a long period of time, i.e. multiple minutes',
      'Remote debugger'
    ];
    // console.ignoredYellowBox = [];
    // this.props.dispatch(email('pass'));
  }
   
  componentDidMount () {
    //SplashScreen.close(SplashScreen.animationType.scale, 850, 500)
    SplashScreen.close({
       animationType: SplashScreen.animationType.scale,
       duration: 850,
       delay: 500,
    });
  }

  componentWillUnmount() {
    console.log('closing app');
  }

  render() {
    //   const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    const store = createStore(reducers, applyMiddleware(...middleware));
    // firebase.database()

    return (
      <Provider store={store}>
        <CurrentChannelListener>
          <ReduxNavigation />
        </CurrentChannelListener>
      </Provider>
    );
  }
}

export default App;
