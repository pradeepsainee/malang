import React, { Component } from 'react';
import { ScrollView, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import { TabNavigator, StackNavigator, addNavigationHelpers } from 'react-navigation';
import NewChannelScreen from './NewChannelScreen';
import LoginScreen from './LoginScreen';
import ChannelScreen from './ChannelScreen';
import ChannelListScreen from './ChannelListScreen';
import { addListener } from '../App';

import { LiveScreen } from './LiveScreen';
import { ShareScreen } from './ShareScreen';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

export const Router = StackNavigator({
  LoginScreen: { screen: LoginScreen },
  ChannelListScreen: { screen: ChannelListScreen },
  CreateChannelScreen: { screen: NewChannelScreen },
  ChannelScreen: { screen: ChannelScreen }
});

class ReduxNavigation extends Component {
  componentWillMount() {
    console.log('component will mount !!');
  }

  render() {
    // const { navigate } = this.props.navigation;
    console.log(`dispatch is${this.props.dispatch}`);
    return (
      <Router
        navigation={addNavigationHelpers({
          state: this.props.nav,
          dispatch: this.props.dispatch,
          addListener
        })}
      />
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    nav: state.navData
  };
};

const mapDispatchToProps = dispatch => ({
  // channelCreate: (text) => dispatch(ChannelCreate(text))
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(ReduxNavigation);
