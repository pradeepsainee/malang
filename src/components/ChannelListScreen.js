import React, { Component } from 'react';
import {
  ScrollView,
  KeyboardAvoidingView,
  Keyboard,
  TouchableHighlight
  // ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { getMyChannelsAction, navigateToCreateChannelScreen } from '../actions/ChannelListActions';
import { SelectCurrentChannelAction } from '../actions/CurrentChannelActions';
import { getSongFeedAction } from '../actions/SongFeedActions';

import {
  Text,
  Screen,
  ListView,
  Tile,
  Title,
  Subtitle,
  Image,
  Divider,
  Heading,
  View,
  Button,
  Row,
  Icon,
  ImageBackground,
  Caption,
  Spinner
} from '@shoutem/ui';
import { TouchableOpacity } from '@shoutem/ui/components/TouchableOpacity';
// import { ImageBackground } from '@shoutem/ui/components/ImageBackground';

// Styles
import { MusicPlayerComponent } from '../components/MusicPlayerComponent';

import firebase from 'firebase';
import { FlatList } from 'react-native-gesture-handler';

import { Router } from './ReduxNavigation';

class ChannelListScreen extends Component {
  static navigationOptions = {
    title: 'Malang',
    headerLeft: null
  };

  componentWillMount() {
    Keyboard.dismiss();
    this.props.getMyChannels();
  }

  renderCreateChannelRow(data) {
    return (
      // <Button onPress={() => this.props.navigateToCreateChannelScreen()}>
      //   <Text>{data.text}</Text>
      // </Button>
      <TouchableOpacity onPress={() => this.props.navigateToCreateChannelScreen()}>
        <Row>
          <Image
            style={{ width: 80, height: 80, borderRadius: 40 }}
            source={{
              uri: 'https://cdn1.iconfinder.com/data/icons/round-black-icons-2/78/plus-128.png'
            }}
          />
          <View styleName="vertical v-center" style={{ flex: 1 }}>
            <Subtitle>{data.text}</Subtitle>
            <Caption>{data.subtext}</Caption>
            {/* <View styleName="horizontal">
              <Subtitle styleName="md-gutter-right"></Subtitle>
              <Caption styleName="line-through">$150.00</Caption>
            </View> */}
          </View>
          {/* <Button styleName="right-icon">
            <Icon name="add-to-cart" />
          </Button> */}
        </Row>
        <Divider styleName="line" />
      </TouchableOpacity>
    );
  }

  renderRow(myChannel) {
    // const { navigate } = this.props.navigation;
    console.log(`the value of this in renderRow is${this}`);
    console.log(`the value of props in renderRow is${JSON.stringify(this.props)}`);
    return (
      <TouchableOpacity onPress={() => this.props.selectChannel(myChannel.channelId)}>
        <Row>
          <Image
            style={{ width: 80, height: 80, borderRadius: 40 }}
            source={{ uri: myChannel.photo }}
          />
          <View styleName="vertical v-center">
            <Subtitle>{myChannel.name}</Subtitle>
            {/* <View styleName="horizontal">
              <Subtitle styleName="md-gutter-right">$120.00</Subtitle>
              <Caption styleName="line-through">$150.00</Caption>
            </View> */}
          </View>
          {/* <Button styleName="right-icon">
            <Icon name="add-to-cart" />
          </Button> */}
        </Row>
      </TouchableOpacity>
    );
  }

  // renderRowHorizontal(publicChannels) {
  //   // const image = require(publicChannels.image);
  //   return (
  //     <View>
  //       <ImageBackground style={{ height: 140, width: 120 }} source={publicChannels.image}>
  //         <Tile>
  //           <Title styleName="md-gutter-bottom">{publicChannels.name}</Title>
  //           <Subtitle styleName="sm-gutter-horizontal">{publicChannels.genre}</Subtitle>
  //         </Tile>
  //       </ImageBackground>
  //       <Divider styleName="line" />
  //     </View>
  //   );
  // }

  //shared by people
  renderRowHorizontal(song) {
    // const image = require(publicChannels.image);
    return (
      <TouchableOpacity onPress={{}}>
        <ImageBackground style={{ height: 140, width: 120 }} source={song.image}>
          <Tile>
            <Title styleName="md-gutter-bottom">{song.name}</Title>
            <Subtitle styleName="sm-gutter-horizontal">{song.sharee_name}</Subtitle>
          </Tile>
        </ImageBackground>
        <Divider styleName="line" />
      </TouchableOpacity>
    );
  }

  render() {
    const publicChannels = [
      { name: 'pc1', genre: 'love', image: require('../images/G4-3.jpg') },
      { name: 'pc2', genre: 'pop', image: require('../images/Where_are_u_now.jpg') },
      { name: 'pc3', genre: 'party', image: require('../images/3-Peg.jpg') },
      { name: 'pc4', genre: 'edm', image: require('../images/titanium.jpg') }
    ];

    console.log(`the value of this in render is${this}`);
    return (
      <Screen styleName="paper">
        <ScrollView style={{ flex: 1 }}>
          {/* <View
            styleName="horizontal h-start v-center"
            style={{ paddingTop: 10, paddingBottom: 10 }}
          >
            <Subtitle style={{ left: 20 }}>Song Feed</Subtitle>
          </View> */}
          {/* <ListView horizontal data={this.props.songfeed} renderRow={this.renderRowHorizontal} /> */}

          {/* <Divider styleName="section-header" style={{ backgroundColor: 'white' }}> */}
          <View
            styleName="horizontal h-start v-center"
            style={{ paddingTop: 10, paddingBottom: 10 }}
          >
            <Subtitle style={{ left: 20 }}>My Channels</Subtitle>
          </View>
          {/* </Divider> */}
          {/* <Button onPress={() => this.props.navigateToCreateChannelScreen()}>
            <Text>Go To Create Channel Screen</Text>
          </Button> */}

          <FlatList
            data={[
              {
                text: 'Create A Channel',
                subtext: 'start listening and sharing music with friends'
              }
            ]}
            renderItem={({ item }) => (
              <TouchableOpacity onPress={() => this.props.selectChannel(item.channelId)}>
                <Row>
                  <Image
                    style={{ width: 80, height: 80, borderRadius: 40 }}
                    source={{ uri: item.photo }}
                  />
                  <View styleName="vertical v-center">
                    <Subtitle>{item.name}</Subtitle>
                    {/* <View styleName="horizontal">
              <Subtitle styleName="md-gutter-right">$120.00</Subtitle>
              <Caption styleName="line-through">$150.00</Caption>
            </View> */}
                  </View>
                  {/* <Button styleName="right-icon">
            <Icon name="add-to-cart" />
          </Button> */}
                </Row>
              </TouchableOpacity>
            )}
          />

          {this.props.showSpinner && (
            <View style={{ zIndex: 2 }}>
              <Spinner />
            </View>
          )

          // alert('show spinner')
          }
          <ListView data={this.props.myChannels} renderRow={this.renderRow.bind(this)} />

          {/* <Button
            onPress={() => {
              firebase
                .auth()
                .signOut()
                .then(() => {})
                .catch(error => {
                  // An error happened.
                  console.log(`error signing out${error}`);
                });
            }}
          >
            <Text>Hi</Text>
          </Button> */}
        </ScrollView>
        {Object.keys(this.props.currentSong).length > 0 && (
          <MusicPlayerComponent
            trackName={this.props.currentSong.trackName}
            uploaderName={this.props.currentSong.uploaderName}
            songPic={this.props.currentSong.currentSongPicUrl}
          />
        )}
      </Screen>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    myChannels: state.channelListData.myChannels,
    publicChannels: state.channelListData.myChannels,
    showSpinner: state.spinner.showSpinner,
    currentSong: state.currentChannelData.currentSong
    // songFeed: state.songFeed.feed
  };
};

const mapDispatchToProps = dispatch => ({
  getMyChannels: () => dispatch(getMyChannelsAction()),
  navigateToCreateChannelScreen: () => dispatch(navigateToCreateChannelScreen()),
  // navigateToCreateChannelScreen: () => dispatch(navigateToCreateChannelScreenAction()),
  // selectChannel: channelId => dispatch(SelectCurrentChannelAction(channelId))
  selectChannel: () => dispatch(Router.router.getActionForPathAndParams('ChannelScreen'))
  // getSongFeed: userId => dispatch(getSongFeedAction(userId))
});

export default connect(mapStateToProps, mapDispatchToProps)(ChannelListScreen);
