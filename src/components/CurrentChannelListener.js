import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { TextInput, Title, Screen, View, NavigationBar, Card, Button, Text } from '@shoutem/ui';

import firebase from 'firebase';

// const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken.toString());

class CurrentChannelListener extends Component {
  componentWillUpdate() {
    console.log('online listener will mount again!');

    // const channelId = this.props.currentChannelId;
    if (firebase.auth().currentUser) {
      const userId = firebase.auth().currentUser.uid;
      const connectedRef = firebase.database().ref('.info/connected');
      connectedRef.on('value', snap => {
        if (snap.val() === true) {
          console.log(`connected and channelid is${this.props.currentChannelId}`);
          if (this.props.currentChannelId !== '') {
            console.log('updating listener');
            firebase
              .database()
              .ref(`channels/${this.props.currentChannelId}/listeners/${userId}`)
              .update({
                name: firebase.auth().currentUser.email,
                time_added: firebase.database.ServerValue.TIMESTAMP
              });
          }
        } else {
          console.log('not connected');
        }
      });
    }
  }

  // componentDidMount() {}

  render() {
    return this.props.children;
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    currentChannelId: state.currentChannelData.channelId
  };
};

const mapDispatchToProps = dispatch => ({
  //   emailChanged: email => dispatch(emailChanged(email)),
  //   passwordChanged: password => dispatch(passwordChanged(password)),
  //   loginUser: (email, password) => dispatch(loginUser(email, password)),
  //   createUserNodeFacebook: user => dispatch(createUserNodeFacebook(user)),
  //   loginUserSuccess: user => dispatch(loginUserSuccessFB(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(CurrentChannelListener);
