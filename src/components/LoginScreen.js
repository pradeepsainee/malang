import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  TextInput,
  Title,
  Screen,
  View,
  NavigationBar,
  Card,
  Button,
  Text,
  ImageBackground,
  Subtitle,
  Tile
} from '@shoutem/ui';

import {
  emailChanged,
  passwordChanged,
  loginUser,
  createUserNodeFacebook,
  loginUserSuccessFB
} from '../actions/AuthActions';

import firebase from 'firebase';
const FBSDK = require('react-native-fbsdk');

const { LoginButton, LoginManager, AccessToken, GraphRequest, GraphRequestManager } = FBSDK;
// const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken.toString());

class LoginScreen extends Component {
  static navigationOptions = {
    // title: 'Login/SignUp'
    // static navigationOptions = {
    header: null
    // };
  };

  componentWillMount() {
    // this.props.dispatch(email('pass'));

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        console.log(`user logged${JSON.stringify(user)}`);
        this.props.loginUserSuccess(user);
      } else {
        console.log('blah!');
      }
    });
  }

  componentDidMount() {
    // firebase.auth().onAuthStateChanged(user => {
    //   if (user) console.log(`this should run everytime i am on this screen${user}`);
    // });
  }

  render() {
    const image = require('../images/welcome-screen.jpg');

    return (
      <ImageBackground source={image} style={{ height: '100%', width: '100%' }}>
        {/* <View > */}
        <View style={{ position: 'absolute', bottom: 60 }}>
          <View styleName="horizontal h-center">
            <Text style={{ color: 'white', fontFamily: 'Montserrat-Medium' }} numberOfLines={1}>
              Malang lets you listen music live with friends
            </Text>
          </View>
          <View styleName="horizontal h-center">
            <Subtitle style={{ color: 'white', fontFamily: 'Montserrat-MediumItalic' }}>
              It's just like sharing earphones!
            </Subtitle>
          </View>
          <View styleName="horizontal h-center" style={{ paddingTop: 10 }}>
            <LoginButton
              readPermissions={['user_friends']}
              onLoginFinished={(error, result) => {
                if (error) {
                  console.log(`login has error: ${result.error}`);
                } else if (result.isCancelled) {
                  console.log('login is cancelled.');
                } else {
                  AccessToken.getCurrentAccessToken().then(data => {
                    const credential = firebase.auth.FacebookAuthProvider.credential(
                      data.accessToken.toString()
                    );
                    firebase
                      .auth()
                      .signInWithCredential(credential)
                      .then(
                        user => {
                          console.log('Sign In Success', user);
                          this.props.createUserNodeFacebook(user);
                        },
                        error2 => {
                          console.log('Sign In Error', error2);
                        }
                      );
                  });
                }
              }}
              onLogoutFinished={() => console.log('logout.')}
            />
          </View>
        </View>
        {/* </View> */}
        {/* <Button styleName="secondary" onPress={() => navigate('ScreenTwo')}>
            <Text>Next Page</Text>
          </Button> */}
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    email: state.authData.email,
    password: state.authData.password,
    loginUser: state.authData.loginUser
  };
};

const mapDispatchToProps = dispatch => ({
  emailChanged: email => dispatch(emailChanged(email)),
  passwordChanged: password => dispatch(passwordChanged(password)),
  loginUser: (email, password) => dispatch(loginUser(email, password)),
  createUserNodeFacebook: user => dispatch(createUserNodeFacebook(user)),
  loginUserSuccess: user => dispatch(loginUserSuccessFB(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
