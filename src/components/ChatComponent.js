import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  // TextInput,
  Title,
  Screen,
  // View,
  NavigationBar,
  Card,
  Button,
  Text,
  ImageBackground,
  Subtitle,
  Tile,
  ListView,
  Overlay,
  Row
  //   ListView
} from '@shoutem/ui';

import {
  getChatMessagesAction,
  onChangeChatInputTextAction,
  sendMessageAction
} from '../actions/CurrentChannelActions';

// import { ListView } from 'react-native';

import firebase from 'firebase';
import { FlatList, View, TextInput } from 'react-native';

// const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken.toString());

class ChatComponent extends Component {
  // componentWillMount() {
  //   this.props.getChatMessages(this.props.channelId, this.props.chat);
  // }

  componentDidMount() {}

  _renderChat(chatMessage) {
    return (
      <Row style={{ background: 'red !important' }}>
        <Text style={{ color: 'black', backgroundColor: 'red !important' }}>
          {chatMessage.message}
        </Text>
      </Row>
    );
  }

  render() {
    return (
      <Overlay
        style={{
          height: 200,

          position: 'absolute',
          bottom: 70,
          width: '100%',
          padding: 0
        }}
      >
        <FlatList
          data={this.props.chat}
          renderItem={({ item }) => (
            <View>
              <Text style={{ color: 'white' }}>{item.message}</Text>
            </View>
          )}
          // ref="ListView_Reference"
          // onContentSizeChange={() => {
          //   this.refs.ListView_Reference.scrollToEnd({ animated: true });
          // }}
        />
        {/* <FlatList
          data={[{ key: 'a' }, { key: 'b' }]}
          renderItem={({ item }) => <Text>{item.key}</Text>}
        /> */}

        <View style={{ flexDirection: 'row' }}>
          <TextInput
            style={{ flex: 1, color: 'white' }}
            // style={{  }}
            placeholder="hello"
            value={this.props.message}
            onChangeText={message => this.props.onChangeChatInputText(message)}
          />
          <Button
            style={{ backgroundColor: 'green' }}
            onPress={() => this.props.sendMessage(this.props.channelId, this.props.message)}
          >
            <Text style={{ color: 'white' }}>Send</Text>
          </Button>
        </View>
      </Overlay>
    );
  }
}

const mapStateToProps = state => ({
  message: state.currentChannelData.message,
  chat: state.currentChannelData.chat,
  channelId: state.currentChannelData.channelId
});

const mapDispatchToProps = dispatch => ({
  getChatMessages: (channelId, chat) => dispatch(getChatMessagesAction(channelId, chat)),
  onChangeChatInputText: message => dispatch(onChangeChatInputTextAction(message)),
  sendMessage: (channelId, message) => dispatch(sendMessageAction(channelId, message))
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatComponent);
