import React, { Component } from 'react';
import {
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  View,
  StyleSheet,
  Linking
} from 'react-native';
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import {
  Screen,
  Button,
  Text,
  ListView,
  Divider,
  Row,
  Image,
  Subtitle,
  Caption,
  Spinner
} from '@shoutem/ui';
import {
  CreateChannelAction,
  SelectFriendIdAction,
  SuggestFriendAction,
  getFbFriendsUsingAppAction,
  CreateChannelFbAction
} from '../actions/CreateChannelActions';
import Autocomplete from 'react-native-autocomplete-input';

import Icon from 'react-native-vector-icons/FontAwesome';

class NewChannelScreen extends Component {
  static navigationOptions = {
    title: 'Create Channel'
  };

  componentWillMount() {
    console.log('component will mount !!');
    this.props.getFbFriendsUsingApp();
  }

  renderFbFriendRow(item) {
    console.log(`trying to render row!${JSON.stringify(item)}`);

    const key = Object.keys(item);

    console.log(`key is${key}`);

    return (
      <TouchableOpacity
        onPress={() =>
          this.props.CreateChannelFb(key, item[key].name, item[key].photo, item[key].fb_id)
        }
      >
        <Row>
          <Image
            // style={{ width: 80, height: 80, borderRadius: 40 }}
            styleName="small-avatar"
            source={{ uri: item[key].photo }}
          />
          <View styleName="vertical v-center" style={{ flex: 1 }}>
            <Caption>{item[key].name}</Caption>
          </View>
          <Button styleName="right-icon">
            <Icon name="plus" />
          </Button>
        </Row>
      </TouchableOpacity>
    );
  }

  render() {
    // const { navigate } = this.props.navigation;

    // alert(JSON.stringify(this.props.fbFriends));
    const showSpinner = this.props.showSpinner;
    return (
      <View style={{ flex: 1 }}>
        <Divider styleName="section-header">
          <Caption>SELECT A FRIEND</Caption>
        </Divider>
        <View style={{ flex: 0.8 }}>
          {this.props.fbFriends.length > 0 ? (
            // <ScrollView style={{ flex: 10 }}>
            <ListView data={this.props.fbFriends} renderRow={this.renderFbFriendRow.bind(this)} />
          ) : (
            showSpinner && <Spinner />
            // : (<Text>No friends currently using app? Invite from below</Text>)}
          )}
        </View>
        <View style={{}} />
        <Divider styleName="section-header">
          <Caption>INVITE FRIENDS</Caption>
        </Divider>

        <View
          style={{
            flex: 0.2,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white'
          }}
        >
          {/* <Icon name="whatsapp" size={40} color="#25D366"  /> */}

          <Icon.Button
            name="whatsapp"
            backgroundColor="#25d366"
            onPress={() => Linking.openURL('whatsapp://send?text=hello')}
          >
            Invite via Whatsapp
          </Icon.Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);

  const obj = state.createChannelData.data || {};
  console.log(`obj is${obj}`);

  const result = Object.keys(obj).map(key => [key, obj[key]]);
  console.log(`result is${result}`);
  return {
    text: state.createChannelData.friend,
    data: result,
    selectedFriendId: state.createChannelData.selectedFriendId,
    fbFriends: state.createChannelData.fbFriends,
    showSpinner: state.spinner.showSpinner
  };
};

const mapDispatchToProps = dispatch => ({
  SuggestFriend: text => dispatch(SuggestFriendAction(text)),
  SelectFriendId: (text, selectedFriendId) =>
    dispatch(SelectFriendIdAction(text, selectedFriendId)),
  CreateChannel: (friendId, friend) => dispatch(CreateChannelAction(friendId, friend)),
  getFbFriendsUsingApp: () => dispatch(getFbFriendsUsingAppAction()),
  CreateChannelFb: (friendId, friendName, friendPhoto, friend_fb_id) =>
    dispatch(CreateChannelFbAction(friendId, friendName, friendPhoto, friend_fb_id))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewChannelScreen);
