import React, { Component } from 'react';
import {
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles

// import Icon from 'react-native-vector-icons/FontAwesome';

import {
  Title,
  Subtitle,
  Heading,
  // Button,
  Overlay,
  // Icon,
  Image,
  // ImageBackground,
  Tile,
  NavigationBar,
  ListView,
  Caption,
  Row,
  Spinner,
  Button
} from '@shoutem/ui';
import { Screen } from '@shoutem/ui/components/Screen';
import {
  onSongSearchInputTextChangedAction,
  onSongSuggestionSelectedAction,
  onSongSelectedAction,
  onSongSharedAction,
  onSongDedicatedAction,
  getSongsSharedOrDedicatedAction,
  changeIndexAction,
  getChatMessagesAction
} from '../actions/CurrentChannelActions';

// import { changeIndexAction } from '../actions/TabbedViewActions';
import Icon from 'react-native-vector-icons/FontAwesome';

import firebase from 'firebase';

// import { Player, ReactNativeAudioStreaming } from 'react-native-audio-streaming';
import { MusicPlayerComponent } from '../components/MusicPlayerComponent';
import RNAudioStreamer from 'react-native-audio-streamer';
import Autocomplete from 'react-native-autocomplete-input';

// import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import { TabViewAnimated, TabBar, TabViewPagerExperimental, SceneMap } from 'react-native-tab-view';

import ChatComponent from './ChatComponent';

// type State = NavigationState<
//   Route<{
//     key: string,
//     title: string
//   }>
// >;

class ChannelScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle:
      typeof navigation.state.params === 'undefined' ||
      typeof navigation.state.params.headerTitle === 'undefined' ? (
        // <Image
        //   source={{ uri: 'http://unsplash.it/100x20' }}
        //   style={{ width: 20, height: 20, borderRadius: 10 }}
        //   resizeMode="cover"
        // />
        <Spinner />
      ) : (
        navigation.state.params.headerTitle
      )

    // headerStyle: { backgroundColor: color.theme }
  });

  componentWillMount() {
    console.log('everytime???');
    this.props.getChatMessages(this.props.channelId, this.props.chat);
  }

  componentWillReceiveProps(nextProps) {
    console.log(`hey nextProps are${nextProps}`);
    console.log(this.props.members);

    if (nextProps.members !== this.props.members || nextProps.listeners !== this.props.listeners) {
      const members = nextProps.members;
      const listeners = nextProps.listeners;
      console.log(`listeners in nextprops are${JSON.stringify(listeners)}`);
      this.props.navigation.setParams({
        headerTitle: (
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 1, flexDirection: 'row' }} styleName="horizontal v-center">
              {Object.keys(members).map(key => {
                console.log(members[key]);
                console.log(`listeners inside headertitle are${listeners}`);
                return (
                  <View key={key}>
                    <Image
                      source={{ uri: members[key].pic }}
                      style={{ height: 50, width: 50, borderRadius: 25 }}
                    >
                      {/* {key in listeners && <Subtitle>Online</Subtitle>} */}
                    </Image>
                    {/* <View styleName="horizontal h-center fill-parent"> */}
                    {key in listeners ? (
                      <Caption style={{ color: 'white', position: 'absolute', top: 30, left: 5 }}>
                        Online
                      </Caption>
                    ) : (
                      <Caption style={{ color: 'white', position: 'absolute', top: 30, left: 5 }}>
                        Offline
                      </Caption>
                    )}
                    {/* </View> */}
                  </View>
                );
              })}
            </View>
            <View
              style={{
                paddingRight: 10,
                flexDirection: 'row',

                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Text>Leave</Text>
            </View>
          </View>
        )
      });
    }
  }

  _handleIndexChange = index => this.props.changeIndex(index);

  _renderHeader = props => <TabBar {...props} />;
  _renderChat(chatMessage) {
    return <Text style={{ color: 'black' }}>{chatMessage.message}</Text>;
  }

  renderSharedAndDedSongs(item) {
    return (
      <View>
        <Text>{item.trackName}</Text>
        <Text>{item.streamUrl}</Text>
        <Text />
      </View>
    );
  }

  // _renderPager = props => <TabViewPagerExperimental {...props} />;

  renderRow(searchResult) {
    return (
      <TouchableOpacity>
        <Row>
          <Image styleName="small-avatar top" source={{ uri: searchResult.artwork_url }} />
          <View styleName="vertical">
            <View styleName="horizontal" style={{ paddingRight: 10 }}>
              <Subtitle styleName="multiline">{searchResult.title}</Subtitle>
              <Caption>
                {searchResult.likes_count} <Icon name="heart" />{' '}
              </Caption>
            </View>
            {/* <Text styleName="multiline">{searchResult.title}</Text> */}
            <View
              style={{ flex: 1, flexDirection: 'row' }}
              styleName="horizontal h-start space-between"
            >
              <TouchableOpacity
                onPress={() => {
                  console.log(`channel id ontouchableopacity is${this.props.channelId}`);
                  this.props.onSongSelected(this.props.channelId, searchResult);
                }}
              >
                <Text style={{ paddingRight: 20, color: '#bf0000' }}>Play Live</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  console.log(`channel id ontouchableopacity is${this.props.channelId}`);
                  this.props.onSongShared(this.props.channelId, searchResult);
                }}
              >
                <Text style={{ paddingRight: 20, color: '#008ab8' }}>Share</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  console.log(`channel id ontouchableopacity is${this.props.channelId}`);
                  this.props.onSongDedicated(this.props.channelId, searchResult);
                }}
              >
                <Text style={{ paddingRight: 20, color: '#F50057' }}>Dedicate</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Row>
      </TouchableOpacity>
    );
  }

  renderSharedAndDedSongs(item) {
    return (
      <View>
        <Text>{item.trackName}</Text>
        <Text>{item.streamUrl}</Text>
        <Text />
      </View>
    );
  }

  render() {
    // this.getSCData();

    return (
      <View style={{ flex: 1, backgroundColor: 'transparent' }}>
        <View style={styles.autocompleteContainer}>
          <View style={{ flexDirection: 'row', height: 60 }}>
            <Button
              onPress={() => this.props.changeIndex(0)}
              style={{
                marginRight: 0,
                flex: 0.5,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                // backgroundColor: 'rgba(0,0,0,0.7)',
                borderBottomWidth: 8,
                borderBottomColor: '#007DC4',
                backgroundColor: '#007DC4'
              }}
            >
              <Text style={{ color: 'white' }}>Live</Text>
            </Button>
            <Button
              onPress={() => this.props.changeIndex(1)}
              style={{
                flex: 0.5,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                // color: 'white'
                borderBottomWidth: 3,
                borderBottomColor: '#007DC4',
                backgroundColor: '#007DC4'
              }}
            >
              <Text>Share/Dedicate</Text>
            </Button>
          </View>
          {this.props.tabIndex === 0 && (
            <View style={{ backgroundColor: 'transparent' }}>
              <Autocomplete
                style={{ backgroundColor: 'transparent' }}
                hideResults={this.props.hideResults}
                data={this.props.suggestions}
                placeholder="Search Song to Play.."
                defaultValue={this.props.text}
                onChangeText={text => this.props.onSongSearchInputTextChanged(text)}
                renderItem={item => (
                  <TouchableOpacity
                    onPress={() => this.props.onSongSuggestionSelected(item.output)}
                  >
                    <Text>{item.output}</Text>
                  </TouchableOpacity>
                )}
              />
            </View>
          )}

          {this.props.tabIndex === 1 && (
            <ListView
              data={this.props.sharedAndDedSongs}
              // style={{ paddingBottom: 50 }}
              renderRow={this.renderSharedAndDedSongs}
            />
          )}
        </View>
        {/* {this.props.searchResults.length > 0 ? ( */}
        {/* if(this.props.searchResults.length > 0 ){ */}

        {this.props.currentSong.currentSongPicUrl &&
          this.props.searchResults.length === 0 &&
          this.props.tabIndex === 0 && (
            <View>
              <ImageBackground
                style={{ height: '100%', width: '100%' }}
                source={{
                  uri: this.props.currentSong.currentSongPicUrl.replace(
                    '-large.jpg',
                    '-t500x500.jpg'
                  )
                }}
              >
                <ChatComponent />
              </ImageBackground>
            </View>
          )}

        {this.props.searchResults.length > 0 && (
          <View style={{ marginTop: 40, marginBottom: 50 }}>
            <ListView
              data={this.props.searchResults}
              // style={{ paddingBottom: 50 }}
              renderRow={this.renderRow.bind(this)}
            />
          </View>
        )}
        {Object.keys(this.props.currentSong).length > 0 && (
          <MusicPlayerComponent
            trackName={this.props.currentSong.trackName}
            uploaderName={this.props.currentSong.uploaderName}
            songPic={this.props.currentSong.currentSongPicUrl}
            playedBy={this.props.currentSong.playedBy}
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  text: state.currentChannelData.text,
  suggestions: state.currentChannelData.suggestions,
  searchResults: state.currentChannelData.searchResults || [],
  channelId: state.currentChannelData.channelId,
  hideResults: state.currentChannelData.hideResults,
  currentSong: state.currentChannelData.currentSong,
  members: state.currentChannelData.members,
  listeners: state.currentChannelData.listeners,
  sharedAndDedSongs: state.currentChannelData.sharedAndDedSongs,
  tabProps: state.tabView,
  tabIndex: state.currentChannelData.index
});

const mapDispatchToProps = dispatch => ({
  getChatMessages: (channelId, chat) => dispatch(getChatMessagesAction(channelId, chat)),
  onSongSearchInputTextChanged: text => dispatch(onSongSearchInputTextChangedAction(text)),
  onSongSuggestionSelected: text => dispatch(onSongSuggestionSelectedAction(text)),
  onSongSelected: (channelId, searchResult) =>
    dispatch(onSongSelectedAction(channelId, searchResult)),
  onSongShared: (channelId, searchResult) => dispatch(onSongSharedAction(channelId, searchResult)),
  onSongDedicated: (channelId, searchResult) =>
    dispatch(onSongDedicatedAction(channelId, searchResult))
});

export default connect(mapStateToProps, mapDispatchToProps)(ChannelScreen);

const styles = StyleSheet.create({
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  }
});
