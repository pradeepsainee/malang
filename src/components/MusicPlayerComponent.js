import { View, Image, Row, Text, Caption } from '@shoutem/ui';
import { StyleSheet } from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

export class MusicPlayerComponent extends Component {
  render() {
    return (
      <View
        // styleName="vertical v-center h-start"
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          position: 'absolute',
          bottom: 0,
          height: 70,
          width: '100%',
          // borderTopColor: 'black',
          // borderTopWidth: 1,
          backgroundColor: 'white'
        }}
      >
        {/* <Row
          style={{
            flex: 1,
            borderTopColor: 'black',
            borderTopWidth: 1
          }}
        > */}
        <Image
          style={{ height: 50, width: 50 }}
          source={{
            uri: this.props.songPic
            // 'https://is3-ssl.mzstatic.com/image/thumb/Music82/v4/82/51/26/8251264d-8bd7-7058-c0f6-2f421544f620/8903431633866_cover.jpg/1200x630bb.jpg'
          }}
        />
        <Icon name="play" style={{ paddingLeft: 10 }} size={25} color="#2a2829" />
        <View style={{ paddingLeft: 10, flex: 1 }}>
          <Text numberOfLines={1}>{this.props.trackName}</Text>
          <Caption numberOfLines={1}>{this.props.uploaderName}</Caption>
          <Caption numberOfLines={1}>Played by : {this.props.playedBy}</Caption>
        </View>

        <Image
          style={{ height: 12, width: 105 }}
          source={{
            uri:
              'https://developers.soundcloud.com/assets/logo_black-8c4cb46bf63fda8936f9a4d967416dc6.png'
          }}
        />
        {/* </Row> */}
      </View>
    );
  }
}

// const styles = StyleSheet.create({
//   playerContainer: {
//     height: 20,
//     width: '100%',
//     borderTopColor: 'black',
//     position: 'absolute',
//     bottom: 0
//   }
// });
